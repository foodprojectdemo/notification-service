#!/usr/bin/make

ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

DOCKER_KAFKA=docker-compose exec -T kafka
KAFKA_TOPICS=$(DOCKER_KAFKA) kafka-topics.sh --zookeeper zookeeper:2181
KAFKA_TOPIC_CREATE = $(KAFKA_TOPICS) --create --if-not-exists
EMAIL_TOPIC=--topic notification.emails.v1
BOOTSTRAP_SERVER=--bootstrap-server kafka:9092
BROKER_LIST=--broker-list kafka:9092
KAFKA_PRODUCER=$(DOCKER_KAFKA) kafka-console-producer.sh
KAFKA_CONSUMER=$(DOCKER_KAFKA) kafka-console-consumer.sh

DOCKER_MONGO=docker-compose exec -T notification-service-mongo

TEST_MESSAGE=$(shell cat $(ROOT_DIR)/test-message.json)

kafka-consumer:
	$(KAFKA_CONSUMER) $(BOOTSTRAP_SERVER) $(EMAIL_TOPIC) --from-beginning

send-test-message:
	echo '$(TEST_MESSAGE)' | $(KAFKA_PRODUCER) $(BROKER_LIST) $(EMAIL_TOPIC)

mongo-rs-status:
	$(DOCKER_MONGO) mongo -u notifications -p notifications --authenticationDatabase admin --eval "rs.status();"
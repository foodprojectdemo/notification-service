package com.gitlab.foodprojectdemo.notificationservice.deserializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import com.gitlab.foodprojectdemo.notificationservice.AbstractTest;
import com.gitlab.foodprojectdemo.notificationservice.Faker;
import com.gitlab.foodprojectdemo.notificationservice.brokerproducer.BrokerProducer;
import com.gitlab.foodprojectdemo.notificationservice.testmessage.TestMessage;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static com.gitlab.foodprojectdemo.notificationservice.email.EmailMessageFixture.testMessage;

class DeserializerTest extends AbstractTest {

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private BrokerProducer brokerProducer;

    @InjectMocks
    private Deserializer deserializer;

    @Test
    void shouldDeserialize() throws JsonProcessingException {
        var json = Faker.faker().lorem().characters();
        var testMessage = testMessage();

        when(objectMapper.readValue(json, TestMessage.class)).thenReturn(testMessage);

        var actual = deserializer.deserialize(json, TestMessage.class);

        // noinspection OptionalGetWithoutIsPresent
        assertThat(actual.get()).isEqualTo(testMessage);
    }

    @Test
    void shouldSendToTopicWhenFailedDeserialization() throws JsonProcessingException {
        var json = Faker.faker().lorem().characters();

        when(objectMapper.readValue(json, TestMessage.class)).thenThrow(JsonProcessingException.class);

        var actual = deserializer.deserialize(json, TestMessage.class);

        assertThat(actual.isEmpty()).isTrue();
        verify(brokerProducer).sendToFailedDeserializeTopic(json);
    }
}
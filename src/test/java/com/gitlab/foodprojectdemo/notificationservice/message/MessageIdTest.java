package com.gitlab.foodprojectdemo.notificationservice.message;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import com.gitlab.foodprojectdemo.notificationservice.AbstractTest;

import java.util.UUID;

class MessageIdTest extends AbstractTest {

    @Test
    void shouldBeEqual() {
        var uuid = UUID.randomUUID();
        Assertions.assertThat(new MessageId(uuid)).isEqualTo(new MessageId(uuid));
    }
}
package com.gitlab.foodprojectdemo.notificationservice.brokerproducer;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import com.gitlab.foodprojectdemo.notificationservice.AbstractKafkaTest;
import com.gitlab.foodprojectdemo.notificationservice.Faker;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = BrokerProducer.class)
class BrokerProducerIT extends AbstractKafkaTest {

    @Autowired
    private BrokerProducer brokerProducer;

    @Test
    void shouldSendToFailedDeserializeTopic() {
        var message = Faker.faker().lorem().characters();
        brokerProducer.sendToFailedDeserializeTopic(message);

        Awaitility.await().untilAsserted(() -> {
            var consumerRecord = testConsumer.pop(topics.getFailedDeserializeTopic());
            assertThat(consumerRecord).isNotNull();
            assertThat(consumerRecord.value()).isEqualTo(message);
        });
    }
}
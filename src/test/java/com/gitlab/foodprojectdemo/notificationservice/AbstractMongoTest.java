package com.gitlab.foodprojectdemo.notificationservice;

import org.junit.jupiter.api.Tag;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;
import com.gitlab.foodprojectdemo.notificationservice.config.mongo.MongoConfig;

@Tag("mongo")
@DataMongoTest
@Transactional
@ContextConfiguration(classes = MongoConfig.class)
public abstract class AbstractMongoTest extends AbstractTest {
}

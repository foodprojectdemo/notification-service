package com.gitlab.foodprojectdemo.notificationservice.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class TestConsumer {
    private final Map<String, List<ConsumerRecord<?, ?>>> broker = new ConcurrentHashMap<>();

    public ConsumerRecord<?, ?> pop(String topic) {
        var records = broker.get(topic);
        if (records == null || records.isEmpty()) {
            return null;
        }
        return records.remove(records.size() - 1);
    }

    @KafkaListener(id = "test-listener", topicPattern = ".*", clientIdPrefix = "${spring.kafka.client-id}-test")
    public void listener(ConsumerRecord<?, ?> record, Acknowledgment ack) {
        log.info("Received {}", record);

        broker.compute(record.topic(), (__, list) -> {
            if (list != null) {
                list.add(record);
                return list;
            } else {
                return new ArrayList<>(List.of(record));
            }
        });
        ack.acknowledge();
    }
}

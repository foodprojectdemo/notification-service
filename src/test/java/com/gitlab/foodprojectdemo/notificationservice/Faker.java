package com.gitlab.foodprojectdemo.notificationservice;

public class Faker {
    private static final com.github.javafaker.Faker faker = new com.github.javafaker.Faker();

    public static com.github.javafaker.Faker faker() {
        return faker;
    }
}

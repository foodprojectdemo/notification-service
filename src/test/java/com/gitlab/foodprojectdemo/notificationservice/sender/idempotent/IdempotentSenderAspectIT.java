package com.gitlab.foodprojectdemo.notificationservice.sender.idempotent;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import com.gitlab.foodprojectdemo.notificationservice.AbstractMongoTest;
import com.gitlab.foodprojectdemo.notificationservice.config.retry.RetryTemplateConfig;
import com.gitlab.foodprojectdemo.notificationservice.sender.Sender;
import com.gitlab.foodprojectdemo.notificationservice.testmessage.TestMessage;

import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.assertThat;
import static com.gitlab.foodprojectdemo.notificationservice.email.EmailMessageFixture.testMessage;

@TestComponent
class Counter {
    final AtomicInteger count = new AtomicInteger(0);
}

@Slf4j
@TestComponent
@AllArgsConstructor
class TestMessageSender implements Sender<TestMessage> {
    private final Counter counter;

    @SneakyThrows
    @Override
    public void send(TestMessage message) {
        log.info(String.valueOf(counter.count));
        Thread.sleep(1000);
        counter.count.incrementAndGet();
        log.info("The message has been sent");
    }
}

@Slf4j
@ComponentScan
@ContextConfiguration(classes = { RetryTemplateConfig.class })
@EnableAspectJAutoProxy
class IdempotentSenderAspectIT extends AbstractMongoTest {

    @Autowired
    Counter counter;

    @Autowired
    TestMessageSender testMessageSender;

    @Test
    void shouldSendMessageOnlyOnce() throws InterruptedException {
        var testMessage = testMessage();

        Runnable runnable1 = () -> testMessageSender.send(testMessage);
        Runnable runnable2 = () -> testMessageSender.send(testMessage);

        Thread thread1 = new Thread(runnable1);
        Thread thread2 = new Thread(runnable2);
        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        assertThat(counter.count.get()).isEqualTo(1);
    }
}
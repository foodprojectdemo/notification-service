package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import com.gitlab.foodprojectdemo.notificationservice.AbstractTest;
import com.gitlab.foodprojectdemo.notificationservice.Faker;
import com.gitlab.foodprojectdemo.notificationservice.testmessage.TestMessage;

import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static com.gitlab.foodprojectdemo.notificationservice.email.EmailMessageFixture.messageId;
import static com.gitlab.foodprojectdemo.notificationservice.email.EmailMessageFixture.testMessage;

class RetrySchedulerTest extends AbstractTest {

    @Mock
    private DelayedMessageRepository delayedMessageRepository;

    @Mock
    private NextRetryTimeCalculator<TestMessage> calculator;

    private RetryScheduler<TestMessage> retryScheduler;

    private int maxRetryNumber;

    @Mock
    private Throwable anyThrowable;

    @BeforeEach
    void setUp() {
        maxRetryNumber = Faker.faker().number().numberBetween(5, 10);
        retryScheduler = new RetryScheduler<>(delayedMessageRepository, maxRetryNumber, calculator);
    }

    @Test
    void shouldSaveDelayedMessage() {
        var testMessage = testMessage();
        var nextRetryTime = ZonedDateTime.now();

        when(calculator.calculateNextRetryTime(testMessage)).thenReturn(nextRetryTime);

        assertThat(testMessage.getRetryNumber()).isLessThan(maxRetryNumber);
        retryScheduler.scheduleRetry(testMessage, anyThrowable);

        verify(delayedMessageRepository).save(new DelayedMessage(testMessage, nextRetryTime));
    }

    @Test
    void shouldNotSaveDelayedMessageWhenRetriesExpired() {
        var testMessage = new TestMessage(messageId(), maxRetryNumber);

        retryScheduler.scheduleRetry(testMessage, anyThrowable);

        verify(delayedMessageRepository, never()).save(any());
    }

    @Test
    void shouldNotSaveDelayedMessageWhenThrowableCannotBeRetry() {
        var testMessage = testMessage();

        retryScheduler = new RetryScheduler<>(delayedMessageRepository, maxRetryNumber, calculator) {
            @Override
            protected boolean isRetriable(TestMessage message, Throwable throwable) {
                return false;
            }
        };

        assertThat(testMessage.getRetryNumber()).isLessThan(maxRetryNumber);
        retryScheduler.scheduleRetry(testMessage, anyThrowable);

        verify(delayedMessageRepository, never()).save(any());
    }
}
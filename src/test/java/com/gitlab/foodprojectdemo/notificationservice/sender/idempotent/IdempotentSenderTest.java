package com.gitlab.foodprojectdemo.notificationservice.sender.idempotent;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import com.gitlab.foodprojectdemo.notificationservice.AbstractTest;
import com.gitlab.foodprojectdemo.notificationservice.testmessage.TestMessage;

import java.util.Optional;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

class IdempotentSenderTest extends AbstractTest {

    @Mock
    private SentMessageRepository sentMessageRepository;

    @Mock
    private ThrowingRunnable throwingRunnable;

    @InjectMocks
    private IdempotentSender idempotentSender;

    @Test
    void shouldSendWhenSentMessageIsNotPresentInRepo(TestMessage testMessage) throws Throwable {
        Mockito.when(sentMessageRepository.findById(testMessage.getId())).thenReturn(Optional.empty());

        idempotentSender.send(throwingRunnable, testMessage);

        verify(throwingRunnable).run();
        verify(sentMessageRepository).save(new SentMessage(testMessage.getId()));
    }

    @Test
    void shouldNotSendWhenSentMessageIsPresentInRepo(TestMessage testMessage) throws Throwable {
        Mockito.when(sentMessageRepository.findById(testMessage.getId()))
                .thenReturn(Optional.of(new SentMessage(testMessage.getId())));

        idempotentSender.send(throwingRunnable, testMessage);

        verify(throwingRunnable, never()).run();
        verify(sentMessageRepository, never()).save(new SentMessage(testMessage.getId()));
    }
}
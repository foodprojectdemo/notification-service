package com.gitlab.foodprojectdemo.notificationservice.sender.idempotent;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.gitlab.foodprojectdemo.notificationservice.AbstractMongoTest;
import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

import static org.assertj.core.api.Assertions.assertThat;

class MongoSentDelayedMessageRepositoryIT extends AbstractMongoTest {
    @Autowired
    private SentMessageRepository sentMessageRepository;

    @Test
    void saveAndFindById() {
        var sentMessage = new SentMessage(new MessageId());

        sentMessageRepository.save(sentMessage);
        var actual = sentMessageRepository.findById(sentMessage.getId());

        assertThat(actual.isPresent()).isTrue();
        assertThat(actual.get()).isEqualTo(sentMessage);
    }
}
package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import com.gitlab.foodprojectdemo.notificationservice.AbstractTest;
import com.gitlab.foodprojectdemo.notificationservice.sender.Sender;
import com.gitlab.foodprojectdemo.notificationservice.testmessage.TestMessage;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static com.gitlab.foodprojectdemo.notificationservice.email.EmailMessageFixture.testMessage;

@Slf4j
@TestComponent
class TestMessageSender implements Sender<TestMessage> {

    @Override
    public void send(TestMessage message) {
        throw new RuntimeException();
    }
}

@Slf4j
@EnableAspectJAutoProxy
@SpringBootTest
@ContextConfiguration(classes = { RetrySenderAspect.class, TestMessageSender.class })
class RetrySenderAspectTest extends AbstractTest {

    @Autowired
    private Sender<TestMessage> sender;

    @MockBean
    private RetryScheduler<TestMessage> retryScheduler;

    @Test
    void shouldRetry() {
        var testMessage = testMessage();

        sender.send(testMessage);
        verify(retryScheduler).scheduleRetry(eq(testMessage), any());
    }
}
package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import com.gitlab.foodprojectdemo.notificationservice.AbstractMongoTest;
import com.gitlab.foodprojectdemo.notificationservice.email.EmailMessageParameterResolver;
import com.gitlab.foodprojectdemo.notificationservice.testmessage.TestMessage;

import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(EmailMessageParameterResolver.class)
class MongoDelayedMessageRepositoryIT extends AbstractMongoTest {

    @Autowired
    private DelayedMessageRepository delayedMessageRepository;

    @Test
    void saveDelayedTestMessage(TestMessage testMessage) {
        var delayMessage = new DelayedMessage(testMessage, ZonedDateTime.now());

        delayedMessageRepository.save(delayMessage);
        var actual = delayedMessageRepository.findById(delayMessage.getMessage().getId());

        assertThat(actual.isPresent()).isTrue();
        assertThat(actual.get()).isEqualTo(delayMessage);
    }

    @Test
    void shouldReturnDelayedMessageByThisTime(TestMessage testMessage) {
        var delayMessage = new DelayedMessage(testMessage, ZonedDateTime.now());

        delayedMessageRepository.save(delayMessage);
        var messages = delayedMessageRepository.findActualDelayedMessage();

        assertThat(messages).contains(delayMessage);
    }

    @Test
    void shouldNotReturnDelayedMessageByThisTime(TestMessage testMessage) {
        var delayMessage = new DelayedMessage(testMessage, ZonedDateTime.now().plusMinutes(1));

        delayedMessageRepository.save(delayMessage);
        var messages = delayedMessageRepository.findActualDelayedMessage();

        assertThat(messages).doesNotContain(delayMessage);
    }

}
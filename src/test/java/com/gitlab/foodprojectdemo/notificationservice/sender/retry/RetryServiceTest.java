package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import com.gitlab.foodprojectdemo.notificationservice.AbstractTest;
import com.gitlab.foodprojectdemo.notificationservice.brokerproducer.BrokerProducer;

import java.time.ZonedDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static com.gitlab.foodprojectdemo.notificationservice.email.EmailMessageFixture.testMessage;

class RetryServiceTest extends AbstractTest {

    @Mock
    private DelayedMessageRepository delayedMessageRepository;

    @Mock
    private BrokerProducer brokerProducer;

    @InjectMocks
    private RetryService retryService;

    @Test
    void shouldResendDelayedMessages() {
        var testMessage = testMessage();
        var retryNumber = testMessage.getRetryNumber();
        var delayedMessages = List.of(new DelayedMessage(testMessage, ZonedDateTime.now()));
        when(delayedMessageRepository.findActualDelayedMessage()).thenReturn(delayedMessages);

        retryService.resendDelayedMessages();

        assertThat(testMessage.getRetryNumber()).isEqualTo(retryNumber + 1);
        verify(brokerProducer).sendToEmailsTopic(testMessage);
        verify(delayedMessageRepository).deleteById(testMessage.getId());
    }

    @Test
    void shouldNotDeleteMessageOnThrowable() {
        var testMessage = testMessage();
        var delayedMessages = List.of(new DelayedMessage(testMessage, ZonedDateTime.now()));

        when(delayedMessageRepository.findActualDelayedMessage()).thenReturn(delayedMessages);
        doThrow(RuntimeException.class).when(brokerProducer).sendToEmailsTopic(testMessage);

        retryService.resendDelayedMessages();

        verify(delayedMessageRepository, never()).deleteById(testMessage.getId());
    }
}
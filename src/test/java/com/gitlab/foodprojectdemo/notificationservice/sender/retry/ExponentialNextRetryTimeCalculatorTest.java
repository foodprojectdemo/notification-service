package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import lombok.Getter;
import lombok.ToString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import com.gitlab.foodprojectdemo.notificationservice.AbstractTest;
import com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage;
import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import static org.assertj.core.api.Assertions.assertThat;

class ExponentialNextRetryTimeCalculatorTest extends AbstractTest {

    @Getter
    @ToString(callSuper = true)
    private static class TestMessage extends AbstractMessage {
        public TestMessage(MessageId id, int retryNumber) {
            super(id, retryNumber);
        }
    }

    @BeforeEach
    void setUp() {
        var clock = Clock.fixed(Instant.now(), ZoneId.systemDefault());
        SystemTime.getInstance().setClock(clock);
    }

    @ParameterizedTest
    @CsvSource({ "0, 10", // 10sec
            "1, 60", // 1min
            "2, 360", // 6min
            "3, 2160", // 36min
            "4, 12960", // 3,6h
            "5, 77760", // 21,6h
            "6, 86400", // 24h
    })
    void calculateNextRetryTime(int retryNumber, long result) {
        var calculator = new ExponentialNextRetryTimeCalculator<TestMessage>(10, 6, 86400);
        var actual = calculator.calculateNextRetryTime(new TestMessage(new MessageId(), retryNumber));

        assertThat(actual).isEqualTo(SystemTime.getInstance().now().plusSeconds(result));
    }
}
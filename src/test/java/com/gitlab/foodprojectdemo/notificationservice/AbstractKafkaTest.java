package com.gitlab.foodprojectdemo.notificationservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import com.gitlab.foodprojectdemo.notificationservice.brokerproducer.Topics;
import com.gitlab.foodprojectdemo.notificationservice.config.jackson.JacksonConfig;
import com.gitlab.foodprojectdemo.notificationservice.consumer.TestConsumer;

@Tag("kafka")
@EnableKafka
@SpringBootTest
@ContextHierarchy(@ContextConfiguration(name = "kafka-configuration", classes = { JacksonConfig.class,
        JacksonAutoConfiguration.class, KafkaAutoConfiguration.class, TestConsumer.class }))
@EnableConfigurationProperties(Topics.class)
public abstract class AbstractKafkaTest extends AbstractTest {

    @Autowired
    protected Topics topics;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected TestConsumer testConsumer;

    @Autowired
    protected KafkaTemplate<String, String> kafkaTemplate;
}

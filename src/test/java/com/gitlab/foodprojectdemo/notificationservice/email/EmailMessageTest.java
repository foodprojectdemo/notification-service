package com.gitlab.foodprojectdemo.notificationservice.email;

import org.junit.jupiter.api.Test;
import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static com.gitlab.foodprojectdemo.notificationservice.email.EmailMessageFixture.emailMessage;

class EmailMessageTest {
    @Test
    void shouldBeEqual() {
        var messageId = new MessageId(UUID.randomUUID());
        var emailMessage1 = emailMessage(messageId);
        var emailMessage2 = emailMessage(messageId);
        assertThat(emailMessage1).isEqualTo(emailMessage2);
    }
}
package com.gitlab.foodprojectdemo.notificationservice.email;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.context.annotation.Import;
import com.gitlab.foodprojectdemo.notificationservice.config.jackson.JacksonConfig;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static com.gitlab.foodprojectdemo.notificationservice.email.EmailMessageFixture.emailMessage;

@Slf4j
@JsonTest
@Import(JacksonConfig.class)
class EmailMessageJsonTest {

    @Autowired
    private JacksonTester<EmailMessage> tester;

    @Test
    void serialize() throws IOException {
        var emailMessage = emailMessage();

        var json = tester.write(emailMessage);

        assertThat(json).extractingJsonPathStringValue("$.id").isEqualTo(emailMessage.getId().toString());
        assertThat(json).extractingJsonPathStringValue("$.subject").isEqualTo(emailMessage.getSubject());
        assertThat(json).extractingJsonPathStringValue("$.text").isEqualTo(emailMessage.getText());
        assertThat(json).extractingJsonPathStringValue("$.toEmail").isEqualTo(emailMessage.getToEmail());
    }

    @Test
    void deserialize() throws IOException {
        var emailMessage = emailMessage();

        var json = String.format("{\"id\":\"%s\",\"subject\":\"%s\",\"text\":\"%s\",\"toEmail\":\"%s\"}",
                emailMessage.getId(), emailMessage.getSubject(), emailMessage.getText(), emailMessage.getToEmail());
        var object = tester.parse(json).getObject();

        assertThat(object.getId()).isEqualTo(emailMessage.getId());
        assertThat(object.getRetryNumber()).isEqualTo(0);
        assertThat(object.getSubject()).isEqualTo(emailMessage.getSubject());
        assertThat(object.getText()).isEqualTo(emailMessage.getText());
        assertThat(object.getToEmail()).isEqualTo(emailMessage.getToEmail());
    }
}

package com.gitlab.foodprojectdemo.notificationservice.email;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import com.gitlab.foodprojectdemo.notificationservice.AbstractKafkaTest;
import com.gitlab.foodprojectdemo.notificationservice.deserializer.Deserializer;
import com.gitlab.foodprojectdemo.notificationservice.sender.Sender;

import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static com.gitlab.foodprojectdemo.notificationservice.Faker.faker;

@ExtendWith(EmailMessageParameterResolver.class)
@ContextConfiguration(classes = EmailMessageListener.class)
class EmailMessageListenerIT extends AbstractKafkaTest {

    @MockBean
    private Deserializer deserializer;

    @MockBean
    private Sender<EmailMessage> sender;

    @Test
    void shouldReceivedTextMessage(EmailMessage emailMessage) {
        var json = faker().lorem().characters();

        when(deserializer.deserialize(json, EmailMessage.class)).thenReturn(Optional.of(emailMessage));

        kafkaTemplate.send(topics.getEmailsTopic(), json);

        Awaitility.await().untilAsserted(() -> verify(sender).send(emailMessage));
    }
}

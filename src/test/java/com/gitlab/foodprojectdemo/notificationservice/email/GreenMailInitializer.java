package com.gitlab.foodprojectdemo.notificationservice.email;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextClosedEvent;

@Slf4j
public class GreenMailInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext context) {
        var greenMail = new GreenMail(new ServerSetup(0, null, ServerSetup.PROTOCOL_SMTP));
        var login = context.getEnvironment().getProperty("spring.mail.username");
        var password = context.getEnvironment().getProperty("spring.mail.password");
        greenMail.setUser(login, password);
        greenMail.start();

        context.getBeanFactory().registerSingleton("greenMail", greenMail);

        context.addApplicationListener(applicationEvent -> {
            if (applicationEvent instanceof ContextClosedEvent) {
                greenMail.stop();
            }
        });

        TestPropertyValues.of("green-mail.server.port=" + greenMail.getSmtp().getPort()).applyTo(context);
    }
}

package com.gitlab.foodprojectdemo.notificationservice.email;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;

public class EmailMessageParameterResolver extends TypeBasedParameterResolver<EmailMessage> {
    @Override
    public EmailMessage resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return EmailMessageFixture.emailMessage();
    }
}

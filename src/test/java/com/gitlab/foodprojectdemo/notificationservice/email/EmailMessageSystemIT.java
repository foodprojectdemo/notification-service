package com.gitlab.foodprojectdemo.notificationservice.email;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.icegreen.greenmail.util.GreenMail;
import lombok.SneakyThrows;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.mail.MailSendException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import com.gitlab.foodprojectdemo.notificationservice.AbstractTest;
import com.gitlab.foodprojectdemo.notificationservice.brokerproducer.BrokerProducer;
import com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage;
import com.gitlab.foodprojectdemo.notificationservice.sender.Sender;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@ExtendWith(EmailMessageParameterResolver.class)
class EmailMessageSystemIT extends AbstractTest {

    @Nested
    @DirtiesContext
    @ContextConfiguration(initializers = GreenMailInitializer.class)
    @SpringBootTest(properties = "spring.mail.port=${green-mail.server.port}")
    class Successful {
        @Autowired
        private KafkaTemplate<String, String> kafkaTemplate;

        @Autowired
        private ObjectMapper objectMapper;

        @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
        @Autowired
        private GreenMail greenMail;

        @Value("${mail.from}")
        private String from;

        @Value("${kafka.topic.emails-topic}")
        private String emailsTopic;

        @Test
        void shouldReceiveEmailMessage(EmailMessage emailMessage) throws IOException {
            kafkaTemplate.send(emailsTopic, objectMapper.writeValueAsString(emailMessage));

            Awaitility.await().untilAsserted(() -> assertThatEmailWasReceived(emailMessage));
        }

        private void assertThatEmailWasReceived(EmailMessage emailMessage) throws MessagingException, IOException {
            var receivedMessages = greenMail.getReceivedMessages();
            assertThat(receivedMessages).isNotEmpty();

            var receivedMessage = receivedMessages[0];
            assertThat(receivedMessage.getSubject()).isEqualTo(emailMessage.getSubject());
            assertThat(receivedMessage.getFrom()).contains(new InternetAddress(from));
            assertThat(receivedMessage.getContent().toString().trim()).isEqualTo(emailMessage.getText());
        }
    }

    @TestConfiguration
    static class FakeSenderConfig {
        private final AtomicBoolean doThrow = new AtomicBoolean(true);
        private final AtomicInteger numberOfSentMessages = new AtomicInteger();

        @Bean
        Sender<EmailMessage> emailMessageSender() {
            return this::throwableSender;
        }

        @SneakyThrows
        void throwableSender(AbstractMessage message) {
            if (doThrow.getAndSet(false)) {
                throw new MailSendException("Sending error");
            }
            numberOfSentMessages.incrementAndGet();
        }
    }

    @Nested
    @DirtiesContext
    @SpringBootTest(properties = "spring.main.allow-bean-definition-overriding=true")
    @Import(FakeSenderConfig.class)
    class WithRetry {

        @Autowired
        private ObjectMapper objectMapper;

        @Autowired
        private KafkaTemplate<String, String> kafkaTemplate;

        @Value("${kafka.topic.emails-topic}")
        private String emailsTopic;

        @SpyBean
        private BrokerProducer brokerProducer;

        @Autowired
        private FakeSenderConfig config;

        @Test
        void shouldReceiveEmailMessage(EmailMessage emailMessage) throws IOException {
            kafkaTemplate.send(emailsTopic, objectMapper.writeValueAsString(emailMessage));
            kafkaTemplate.send(emailsTopic, objectMapper.writeValueAsString(emailMessage));

            Awaitility.await().atMost(Duration.ofSeconds(20)).untilAsserted(() -> {
                verify(brokerProducer).sendToEmailsTopic(emailMessage);
                assertThat(config.numberOfSentMessages.get()).isEqualTo(1);
            });
        }

    }
}

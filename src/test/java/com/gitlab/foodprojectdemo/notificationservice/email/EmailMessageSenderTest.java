package com.gitlab.foodprojectdemo.notificationservice.email;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import com.gitlab.foodprojectdemo.notificationservice.AbstractTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static com.gitlab.foodprojectdemo.notificationservice.Faker.faker;

@ExtendWith(EmailMessageParameterResolver.class)
class EmailMessageSenderTest extends AbstractTest {

    @Mock
    private JavaMailSender javaMailSender;
    private EmailMessageSender emailMessageSender;
    private String from;

    @Captor
    private ArgumentCaptor<SimpleMailMessage> mailMessageArgumentCaptor;

    @BeforeEach
    void setUp() {
        from = faker().internet().emailAddress();
        emailMessageSender = new EmailMessageSender(from, javaMailSender);
    }

    @Test
    void shouldSendEmailMessage(EmailMessage emailMessage) {
        emailMessageSender.send(emailMessage);

        verify(javaMailSender).send(mailMessageArgumentCaptor.capture());

        var mailMessage = mailMessageArgumentCaptor.getValue();

        assertThat(mailMessage.getFrom()).isEqualTo(from);
        assertThat(mailMessage.getTo()).contains(emailMessage.getToEmail());
        assertThat(mailMessage.getSubject()).isEqualTo(emailMessage.getSubject());
        assertThat(mailMessage.getText()).isEqualTo(emailMessage.getText());
    }
}
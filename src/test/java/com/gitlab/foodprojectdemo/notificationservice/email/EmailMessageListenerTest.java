package com.gitlab.foodprojectdemo.notificationservice.email;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.kafka.support.Acknowledgment;
import com.gitlab.foodprojectdemo.notificationservice.AbstractTest;
import com.gitlab.foodprojectdemo.notificationservice.deserializer.Deserializer;
import com.gitlab.foodprojectdemo.notificationservice.sender.Sender;

import java.util.Optional;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static com.gitlab.foodprojectdemo.notificationservice.Faker.faker;

@ExtendWith(EmailMessageParameterResolver.class)
class EmailMessageListenerTest extends AbstractTest {

    @Mock
    private Deserializer deserializer;

    @Mock
    private Sender<EmailMessage> sender;

    @InjectMocks
    private EmailMessageListener emailMessageListener;

    @Mock
    private Acknowledgment acknowledgment;

    @Test
    void shouldSendMessage(EmailMessage emailMessage) {
        var json = faker().lorem().characters();
        when(deserializer.deserialize(json, EmailMessage.class)).thenReturn(Optional.of(emailMessage));

        emailMessageListener.onEmailSourceMessage(json, acknowledgment);

        verify(sender).send(emailMessage);
        verify(acknowledgment).acknowledge();
    }

    @Test
    void shouldHandleSenderExceptions(EmailMessage emailMessage) {
        var json = faker().lorem().characters();
        when(deserializer.deserialize(json, EmailMessage.class)).thenReturn(Optional.of(emailMessage));
        doThrow(RuntimeException.class).when(sender).send(emailMessage);

        emailMessageListener.onEmailSourceMessage(json, acknowledgment);

        verify(acknowledgment).acknowledge();
    }
}

package com.gitlab.foodprojectdemo.notificationservice.email;

import com.icegreen.greenmail.util.GreenMail;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.mail.MailSenderAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import com.gitlab.foodprojectdemo.notificationservice.AbstractTest;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static com.gitlab.foodprojectdemo.notificationservice.email.EmailMessageFixture.emailMessage;

@DirtiesContext
@SpringBootTest
@ContextConfiguration(classes = { EmailMessageSender.class,
        MailSenderAutoConfiguration.class }, initializers = GreenMailInitializer.class)
@TestPropertySource(properties = "spring.mail.port=${green-mail.server.port}")
class EmailMessageSenderIT extends AbstractTest {

    @Autowired
    private EmailMessageSender emailMessageSender;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private GreenMail greenMail;

    @Value("${mail.from}")
    private String from;

    @Test
    void send() throws IOException, MessagingException {
        var emailMessage = emailMessage();
        emailMessageSender.send(emailMessage);

        var receivedMessages = greenMail.getReceivedMessages();
        assertThat(receivedMessages).isNotEmpty();

        var receivedMessage = receivedMessages[0];
        assertThat(receivedMessage.getSubject()).isEqualTo(emailMessage.getSubject());
        assertThat(receivedMessage.getFrom()).contains(new InternetAddress(from));
        assertThat(receivedMessage.getContent().toString().trim()).isEqualTo(emailMessage.getText());
    }
}
package com.gitlab.foodprojectdemo.notificationservice.email;

import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;
import com.gitlab.foodprojectdemo.notificationservice.testmessage.TestMessage;

import java.util.UUID;

import static com.gitlab.foodprojectdemo.notificationservice.Faker.faker;

public class EmailMessageFixture {

    public static MessageId messageId() {
        return new MessageId(UUID.randomUUID());
    }

    public static EmailMessage emailMessage() {
        return new EmailMessage(messageId(), 0, faker().lorem().sentence(), faker().lorem().paragraph(),
                faker().internet().emailAddress());
    }

    public static EmailMessage emailMessage(MessageId messageId) {
        return new EmailMessage(messageId, 0, faker().lorem().sentence(), faker().lorem().paragraph(),
                faker().internet().emailAddress());
    }

    public static TestMessage testMessage() {
        return new TestMessage(messageId());
    }

}

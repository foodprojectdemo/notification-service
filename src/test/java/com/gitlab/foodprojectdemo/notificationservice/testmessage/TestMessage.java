package com.gitlab.foodprojectdemo.notificationservice.testmessage;

import lombok.AllArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.PersistenceConstructor;
import com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage;
import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

@ToString(callSuper = true)
public class TestMessage extends AbstractMessage {

    public TestMessage(MessageId id) {
        super(id, 0);
    }

    @PersistenceConstructor
    public TestMessage(MessageId id, int retryNumber) {
        super(id, retryNumber);
    }
}

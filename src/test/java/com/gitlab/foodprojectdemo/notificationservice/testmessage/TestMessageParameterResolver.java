package com.gitlab.foodprojectdemo.notificationservice.testmessage;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import com.gitlab.foodprojectdemo.notificationservice.email.EmailMessageFixture;

public class TestMessageParameterResolver extends TypeBasedParameterResolver<TestMessage> {
    @Override
    public TestMessage resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return EmailMessageFixture.testMessage();
    }
}

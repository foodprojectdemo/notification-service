package com.gitlab.foodprojectdemo.notificationservice;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ActiveProfiles;
import com.gitlab.foodprojectdemo.notificationservice.testmessage.TestMessageParameterResolver;

@ActiveProfiles("test")
@ExtendWith({ MockitoExtension.class, TestMessageParameterResolver.class })
public abstract class AbstractTest {
}

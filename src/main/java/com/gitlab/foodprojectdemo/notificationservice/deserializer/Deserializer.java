package com.gitlab.foodprojectdemo.notificationservice.deserializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.notificationservice.brokerproducer.BrokerProducer;

import java.util.Optional;

@Slf4j
@Component
@AllArgsConstructor
public class Deserializer {
    private final ObjectMapper objectMapper;
    private final BrokerProducer brokerProducer;

    public <T> Optional<T> deserialize(String json, Class<T> aClass) {
        try {
            return Optional.of(objectMapper.readValue(json, aClass));
        } catch (JsonProcessingException e) {
            log.error(String.format("Deserialization error: %s for json: %s", e.getMessage(), json));
            // TODO: move to another class
            brokerProducer.sendToFailedDeserializeTopic(json);
            return Optional.empty();
        }
    }
}

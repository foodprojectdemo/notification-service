package com.gitlab.foodprojectdemo.notificationservice.email;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.notificationservice.sender.Sender;

@Slf4j
@Component
class EmailMessageSender implements Sender<EmailMessage> {
    private final String from;
    private final JavaMailSender emailSender;

    public EmailMessageSender(@Value("${mail.from}") String from, JavaMailSender emailSender) {
        this.from = from;
        this.emailSender = emailSender;
    }

    @Override
    public void send(EmailMessage emailMessage) {
        var mailMessage = new SimpleMailMessage();

        mailMessage.setFrom(from);
        mailMessage.setTo(emailMessage.getToEmail());
        mailMessage.setSubject(emailMessage.getSubject());
        mailMessage.setText(emailMessage.getText());

        emailSender.send(mailMessage);

        log.info(String.format("Email message with Id %s has been sent ", emailMessage.getId()));
    }
}

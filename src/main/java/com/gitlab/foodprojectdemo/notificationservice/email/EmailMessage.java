package com.gitlab.foodprojectdemo.notificationservice.email;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage;
import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

import java.beans.ConstructorProperties;

@Getter
@ToString(callSuper = true)
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class EmailMessage extends AbstractMessage {
    String subject;
    String text;
    String toEmail;

    @ConstructorProperties({ "id", "retryNumber", "subject", "text", "toEmail", "toName" })
    public EmailMessage(@NonNull MessageId id, int retryNumber, @NonNull String subject, @NonNull String text,
            @NonNull String toEmail) {
        super(id, retryNumber);
        this.subject = subject;
        this.text = text;
        this.toEmail = toEmail;
    }

}

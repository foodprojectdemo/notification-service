package com.gitlab.foodprojectdemo.notificationservice.email;

import org.springframework.mail.MailException;
import org.springframework.mail.MailParseException;
import org.springframework.mail.MailPreparationException;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.notificationservice.sender.retry.DelayedMessageRepository;
import com.gitlab.foodprojectdemo.notificationservice.sender.retry.ExponentialNextRetryTimeCalculator;
import com.gitlab.foodprojectdemo.notificationservice.sender.retry.RetryScheduler;

@Component
class EmailMessageRetryScheduler extends RetryScheduler<EmailMessage> {
    private static final int BASE = 10;
    private static final int MULTIPLIER = 6;
    private static final int MAX_INTERVAL = 86400;

    public EmailMessageRetryScheduler(DelayedMessageRepository delayedMessageRepository) {
        super(delayedMessageRepository, Integer.MAX_VALUE,
                new ExponentialNextRetryTimeCalculator<>(BASE, MULTIPLIER, MAX_INTERVAL));
    }

    @Override
    protected boolean isRetriable(EmailMessage message, Throwable throwable) {
        return throwable instanceof MailException && !(throwable instanceof MailParseException)
                && !(throwable instanceof MailPreparationException);
    }
}

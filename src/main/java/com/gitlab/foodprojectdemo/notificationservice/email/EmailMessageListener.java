package com.gitlab.foodprojectdemo.notificationservice.email;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.notificationservice.deserializer.Deserializer;
import com.gitlab.foodprojectdemo.notificationservice.sender.Sender;

@Slf4j
@Component
@AllArgsConstructor
class EmailMessageListener {
    private final Deserializer deserializer;
    private final Sender<EmailMessage> sender;

    @KafkaListener(groupId = "notification-email-listener", topics = "${kafka.topic.emails-topic}")
    void onEmailSourceMessage(String json, Acknowledgment ack) {
        log.info("Received message: " + json);
        try {
            deserializer.deserialize(json, EmailMessage.class).ifPresent(sender::send);
        } catch (Throwable throwable) {
            log.error(throwable.getMessage(), throwable);
        }
        ack.acknowledge();
    }

}

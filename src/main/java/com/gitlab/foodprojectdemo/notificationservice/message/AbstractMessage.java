package com.gitlab.foodprojectdemo.notificationservice.message;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

@Getter
@ToString
@AllArgsConstructor
public abstract class AbstractMessage {

    @Id
    private final MessageId id;

    private int retryNumber;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        var that = (AbstractMessage) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public void incrementRetryNumber() {
        retryNumber++;
    }
}

package com.gitlab.foodprojectdemo.notificationservice.message;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

import java.util.UUID;

@EqualsAndHashCode
@AllArgsConstructor
public class MessageId {
    private final UUID id;

    public MessageId() {
        this.id = UUID.randomUUID();
    }

    @Override
    public String toString() {
        return id.toString();
    }
}

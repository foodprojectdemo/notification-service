package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.notificationservice.brokerproducer.BrokerProducer;

@Slf4j
@Component
@AllArgsConstructor
class RetryService {
    private final DelayedMessageRepository delayedMessageRepository;
    private final BrokerProducer brokerProducer;

    @Scheduled(fixedDelayString = "${scheduling.fixed-delay-in-milliseconds:3000}")
    public void resendDelayedMessages() {
        log.debug("ResendDelayedMessages");
        delayedMessageRepository.findActualDelayedMessage().forEach(this::resend);
    }

    private void resend(DelayedMessage delayedMessage) {
        log.info("Resending delayed message: {}", delayedMessage);
        var message = delayedMessage.getMessage();
        message.incrementRetryNumber();

        try {
            brokerProducer.sendToEmailsTopic(message);
        } catch (Throwable throwable) {
            log.error(throwable.getMessage(), throwable);
            return;
        }
        delayedMessageRepository.deleteById(message.getId());
    }

}

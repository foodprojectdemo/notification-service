package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import lombok.NonNull;
import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage;
import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

import java.time.ZonedDateTime;

@Value
@Document(collection = "delayed_message")
public class DelayedMessage {

    @Id
    MessageId id;

    AbstractMessage message;

    @NonNull
    ZonedDateTime tryAgainAt;

    @PersistenceConstructor
    DelayedMessage(MessageId id, AbstractMessage message, @NonNull ZonedDateTime tryAgainAt) {
        this.id = id;
        this.message = message;
        this.tryAgainAt = tryAgainAt.withNano(0);
    }

    public DelayedMessage(@NonNull AbstractMessage message, @NonNull ZonedDateTime tryAgainAt) {
        this.id = message.getId();
        this.message = message;
        this.tryAgainAt = tryAgainAt.withNano(0);
    }
}

package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

import java.util.Collection;
import java.util.Optional;

public interface DelayedMessageRepository {

    Optional<DelayedMessage> findById(MessageId id);

    Collection<DelayedMessage> findActualDelayedMessage();

    DelayedMessage save(DelayedMessage delayedMessage);

    void deleteById(MessageId id);
}

package com.gitlab.foodprojectdemo.notificationservice.sender.idempotent;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

interface MongoSentMessageRepository extends MongoRepository<SentMessage, MessageId>, SentMessageRepository {
}

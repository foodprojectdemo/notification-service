package com.gitlab.foodprojectdemo.notificationservice.sender.idempotent;

import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

@Value
@Document(collection = "sent_message")
class SentMessage {
    @Id
    MessageId id;
}

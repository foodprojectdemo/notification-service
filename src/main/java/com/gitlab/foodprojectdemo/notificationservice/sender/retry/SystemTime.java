package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import lombok.Setter;

import java.time.Clock;
import java.time.ZonedDateTime;

public class SystemTime {

    private final static SystemTime instance = new SystemTime();
    @Setter
    private Clock clock = Clock.systemDefaultZone();

    public static SystemTime getInstance() {
        return instance;
    }

    public ZonedDateTime now() {
        return ZonedDateTime.now(clock).withNano(0);
    }
}

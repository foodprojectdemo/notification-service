package com.gitlab.foodprojectdemo.notificationservice.sender;

import com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage;

public interface Sender<T extends AbstractMessage> {
    void send(T message);
}

package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage;

import java.time.ZonedDateTime;

public interface NextRetryTimeCalculator<T extends AbstractMessage> {
    ZonedDateTime calculateNextRetryTime(T message);
}

package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

import java.util.List;

interface MongoDelayedMessageRepository extends CrudRepository<DelayedMessage, MessageId>, DelayedMessageRepository {

    @Override
    @Query("{'tryAgainAt': {$lte: new Date()}}")
    List<DelayedMessage> findActualDelayedMessage();

}

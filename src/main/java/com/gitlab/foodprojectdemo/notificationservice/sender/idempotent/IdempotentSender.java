package com.gitlab.foodprojectdemo.notificationservice.sender.idempotent;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage;

@FunctionalInterface
interface ThrowingRunnable {
    void run() throws Throwable;
}

@Slf4j
@Component
@AllArgsConstructor
class IdempotentSender {
    private final SentMessageRepository sentMessageRepository;

    @Transactional
    @Retryable(DataAccessException.class)
    public <T extends AbstractMessage> void send(ThrowingRunnable throwingRunnable, T message) throws Throwable {
        if (sentMessageRepository.findById(message.getId()).isPresent()) {
            log.info(String.format("The message with Id %s was already sent", message.getId()));
        } else {
            var sentMessage = new SentMessage(message.getId());
            sentMessageRepository.save(sentMessage);

            throwingRunnable.run();
            log.info(String.format("The message[%s] has been sent", message.getId()));
        }
    }

}

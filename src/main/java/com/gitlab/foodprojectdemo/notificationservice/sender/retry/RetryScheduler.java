package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage;

@Slf4j
@AllArgsConstructor
public class RetryScheduler<T extends AbstractMessage> {
    private final DelayedMessageRepository delayedMessageRepository;
    private final int maxRetryNumber;
    private final NextRetryTimeCalculator<T> calculator;

    /**
     * Override to change retry logic, default: always retry
     *
     * @param message
     *            message to send
     * @param throwable
     *            error while sending
     * 
     * @return true - retry, false - skip
     */
    protected boolean isRetriable(T message, Throwable throwable) {
        return true;
    }

    public void scheduleRetry(T message, Throwable throwable) {
        if (!isRetriable(message, throwable) || !canBeDelayed(message)) {
            log.info("The message {} with {} cannot be delayed", message, throwable.toString());
            return;
        }

        var delayedMessage = new DelayedMessage(message, calculator.calculateNextRetryTime(message));
        log.info("DelayedMessage: {}", delayedMessage);
        delayedMessageRepository.save(delayedMessage);
    }

    protected boolean canBeDelayed(T message) {
        if (message.getRetryNumber() < maxRetryNumber) {
            return true;
        } else {
            log.info(String.format("The number of retries has been exhausted for %s", message.getId()));
            return false;
        }
    }

}

package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ResolvableType;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage;

@Slf4j
@Aspect
@Order(1)
@Component
@AllArgsConstructor
class RetrySenderAspect {
    private final ApplicationContext applicationContext;

    @Around("execution(* com.gitlab.foodprojectdemo.notificationservice.sender.Sender.send(com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage))")
    public Object send(ProceedingJoinPoint proceedingJoinPoint) {
        log.info("Sending in RetrySenderAspect");
        var message = ((AbstractMessage) proceedingJoinPoint.getArgs()[0]);

        try {
            proceedingJoinPoint.proceed();
        } catch (Throwable throwable) {
            retry(message, throwable);
        }
        return null;
    }

    private void retry(AbstractMessage message, Throwable throwable) {
        log.info("Retry for {} with {}", message, throwable.toString());
        var type = ResolvableType.forClassWithGenerics(RetryScheduler.class, message.getClass());
        var beanNames = applicationContext.getBeanNamesForType(type);
        if (beanNames.length == 0) {
            log.info("Not found RetryScheduler for {}", message.getClass().getSimpleName());
            return;
        }
        try {
            // noinspection unchecked
            var retryScheduler = (RetryScheduler<AbstractMessage>) applicationContext.getBean(beanNames[0],
                    RetryScheduler.class);
            log.info("Found RetryScheduler: {}", retryScheduler.getClass());
            retryScheduler.scheduleRetry(message, throwable);
        } catch (Throwable ex) {
            log.error(ex.getMessage(), ex);
        }
    }
}

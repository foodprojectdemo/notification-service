package com.gitlab.foodprojectdemo.notificationservice.sender.idempotent;

import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

import java.util.Optional;

interface SentMessageRepository {
    Optional<SentMessage> findById(MessageId messageId);

    SentMessage save(SentMessage sentMessage);
}

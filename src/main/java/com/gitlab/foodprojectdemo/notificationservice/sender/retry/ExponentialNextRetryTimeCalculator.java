package com.gitlab.foodprojectdemo.notificationservice.sender.retry;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage;

import java.time.ZonedDateTime;

@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ExponentialNextRetryTimeCalculator<T extends AbstractMessage> implements NextRetryTimeCalculator<T> {
    int base;
    int multiplier;
    long maxInterval;

    @Override
    public ZonedDateTime calculateNextRetryTime(T message) {
        return SystemTime.getInstance().now().plusSeconds(interval(message));
    }

    private long interval(T message) {
        return Math.min((long) (base * Math.pow(multiplier, message.getRetryNumber())), maxInterval);
    }
}

package com.gitlab.foodprojectdemo.notificationservice.sender.idempotent;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage;

@Slf4j
@Aspect
@Order(2)
@Component
@AllArgsConstructor
class IdempotentSenderAspect {
    private final IdempotentSender idempotentSender;

    @Around("execution(* com.gitlab.foodprojectdemo.notificationservice.sender.Sender.send(com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage))")
    public Object send(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        log.info("Sending in IdempotentSenderAspect");
        var message = ((AbstractMessage) proceedingJoinPoint.getArgs()[0]);

        idempotentSender.send(proceedingJoinPoint::proceed, message);

        return null;
    }

}

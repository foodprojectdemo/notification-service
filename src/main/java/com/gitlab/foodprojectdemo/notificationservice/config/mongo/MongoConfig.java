package com.gitlab.foodprojectdemo.notificationservice.config.mongo;

import com.mongodb.MongoClientSettings.Builder;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.autoconfigure.mongo.MongoPropertiesClientSettingsBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Configuration
@ComponentScan
@EnableMongoRepositories("com.gitlab.foodprojectdemo.notificationservice.sender")
public class MongoConfig {

    @Bean
    public MongoPropertiesClientSettingsBuilderCustomizer mongoPropertiesClientSettingsBuilderCustomizer(
            MongoProperties mongoProperties, Environment environment) {
        return new MongoPropertiesClientSettingsBuilderCustomizer(mongoProperties, environment) {
            @Override
            public void customize(Builder settingsBuilder) {
                settingsBuilder
                        .applyToClusterSettings(builder -> builder.serverSelectionTimeout(5000, TimeUnit.MILLISECONDS));
            }
        };
    }

    @Bean
    public MongoCustomConversions mongoCustomConversions(List<Converter<?, ?>> converters) {
        return new MongoCustomConversions(converters);
    }

    @Bean
    public MongoTransactionManager transactionManager(MongoDatabaseFactory mongoDatabaseFactory) {
        return new MongoTransactionManager(mongoDatabaseFactory);
    }

    @Bean
    public TransactionTemplate transactionTemplate(MongoTransactionManager mongoTransactionManager) {
        return new TransactionTemplate(mongoTransactionManager);
    }

}

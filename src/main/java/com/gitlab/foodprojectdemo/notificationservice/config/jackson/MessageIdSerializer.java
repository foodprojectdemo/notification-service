package com.gitlab.foodprojectdemo.notificationservice.config.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;
import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

import java.io.IOException;

@JsonComponent
class MessageIdSerializer extends JsonSerializer<MessageId> {

    @Override
    public void serialize(MessageId messageId, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException {
        jsonGenerator.writeString(messageId.toString());
    }
}

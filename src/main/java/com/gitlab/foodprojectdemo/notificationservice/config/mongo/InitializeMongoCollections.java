package com.gitlab.foodprojectdemo.notificationservice.config.mongo;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Slf4j
@Component
@AllArgsConstructor
class InitializeMongoCollections {
    private static final String SENT_MESSAGE_COLLECTION = "sent_message";
    private static final String DELAYED_MESSAGE_COLLECTION = "delayed_message";
    private final MongoTemplate mongoTemplate;

    @PostConstruct
    public void createCollections() {
        List.of(SENT_MESSAGE_COLLECTION, DELAYED_MESSAGE_COLLECTION).forEach(collectionName -> {
            if (!mongoTemplate.collectionExists(collectionName)) {
                mongoTemplate.createCollection(collectionName);
            }
        });
    }

}

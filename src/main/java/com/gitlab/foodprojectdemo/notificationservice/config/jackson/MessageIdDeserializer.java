package com.gitlab.foodprojectdemo.notificationservice.config.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.boot.jackson.JsonComponent;
import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

import java.io.IOException;
import java.util.UUID;

@JsonComponent
class MessageIdDeserializer extends JsonDeserializer<MessageId> {

    @Override
    public MessageId deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        return new MessageId(UUID.fromString(node.textValue()));
    }
}

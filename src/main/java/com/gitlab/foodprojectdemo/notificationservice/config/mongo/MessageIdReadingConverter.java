package com.gitlab.foodprojectdemo.notificationservice.config.mongo;

import lombok.NonNull;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

import java.util.UUID;

@Component
@ReadingConverter
class MessageIdReadingConverter implements Converter<String, MessageId> {

    @Override
    public MessageId convert(@NonNull String messageId) {
        return new MessageId(UUID.fromString(messageId));
    }
}

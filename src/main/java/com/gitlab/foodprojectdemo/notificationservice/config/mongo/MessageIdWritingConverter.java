package com.gitlab.foodprojectdemo.notificationservice.config.mongo;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.notificationservice.message.MessageId;

@Component
@WritingConverter
class MessageIdWritingConverter implements Converter<MessageId, String> {

    @Override
    public String convert(MessageId messageId) {
        return messageId.toString();
    }
}

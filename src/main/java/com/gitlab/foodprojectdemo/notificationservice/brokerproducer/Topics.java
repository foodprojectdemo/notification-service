package com.gitlab.foodprojectdemo.notificationservice.brokerproducer;

import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@Value
@ConstructorBinding
@ConfigurationProperties("kafka.topic")
public class Topics {
    String emailsTopic;
    String failedDeserializeTopic;
}

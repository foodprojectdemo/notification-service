package com.gitlab.foodprojectdemo.notificationservice.brokerproducer;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import com.gitlab.foodprojectdemo.notificationservice.message.AbstractMessage;

@Slf4j
@Service
@AllArgsConstructor
@EnableConfigurationProperties(Topics.class)
public class BrokerProducer {
    private final Topics topics;
    private final KafkaTemplate<String, String> kafkaTemplate;
    private final ObjectMapper objectMapper;

    @SneakyThrows
    public void sendToFailedDeserializeTopic(String sourceMessage) {
        kafkaTemplate.send(topics.getFailedDeserializeTopic(), sourceMessage).get();
    }

    @SneakyThrows
    public void sendToEmailsTopic(AbstractMessage message) {
        kafkaTemplate.send(topics.getEmailsTopic(), objectMapper.writeValueAsString(message)).get();
    }

}

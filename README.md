[![pipeline status](https://gitlab.com/foodprojectdemo/notification-service/badges/main/pipeline.svg)](https://gitlab.com/foodprojectdemo/notification-service/-/pipelines)
[![UT coverage report](https://gitlab.com/foodprojectdemo/notification-service/badges/main/coverage.svg?job=test-ut)](https://foodprojectdemo.gitlab.io/notification-service/coverage/ut/)
[![IT coverage report](https://gitlab.com/foodprojectdemo/notification-service/badges/main/coverage.svg?job=test-it)](https://foodprojectdemo.gitlab.io/notification-service/coverage/it/)


# Notification service

Notification service - a microservice for notification sending. There are only email notifications. The notification
service supports reliable idempotent and retry sending.

## Email message example

```json
{
  "id": "e7479ccc-e38a-449b-9857-85b1e142396e",
  "subject": "Message subject",
  "text": "Message body",
  "toEmail": "my-test@mail.com"
}
```

## Using topics

- **notification.emails.v1** - messages should be sent here
- **notification.failed-deserialize.v1** - channel for messages that could not be deserialized

## Manual testing

You can play with the service.

1. For it, run infrastructure:

```shell
docker-compose up -d
```

2. Go to browser to [MailDev](http://localhost:1080) for to view receiving messages.


3. Then run the service with the following command:

```shell
./mvnw spring-boot:run -DskipTests -Dspring-boot.run.profiles=dev 
```

4. Send the test message:

```shell
make send-test-message
```

5. The sent message should appear in [MailDev](http://localhost:1080).

If you execute the send command again, you won`t see any new message, because the message with the same ID have been
sent already.

## Technical Overview

![images/notification-sequence.png](images/notification-sequence.png)

![images/notification-classes.png](images/notification-classes.png)

### Idempotent Sender Aspect

The [IdempotentSenderAspect](src/main/java/com/gitlab/foodprojectdemo/notificationservice/sender/idempotent/IdempotentSenderAspect.java)
uses Around advice to intercept the ``Sender.send`` call and verifies that the message was sent only once. The aspect
saves sent ``messageId`` if the message was sent successfully.

### Retry Sender Aspect

The [IdempotentSenderAspect](src/main/java/com/gitlab/foodprojectdemo/notificationservice/sender/retry/RetrySenderAspect.java)
uses Around advice to intercept the ``Sender.send`` call, catches all exceptions and uses
the [RetryScheduler](src/main/java/com/gitlab/foodprojectdemo/notificationservice/sender/retry/RetryScheduler.java) to
schedule retry for delayed message. 